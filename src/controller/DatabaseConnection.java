package controller;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * {@code Class DatabaseConnection} provides a connection to H2 database.
 *
 * @author Francesco Battaglia.
 * */
public class DatabaseConnection {
    private Connection conn; // Connection object.

    /**
     * Create a new connection to an H2 Database.
     * */
    public DatabaseConnection() {
        try {
            // Set a connection to an H2 Database using the System Environment Variables.
            this.conn = DriverManager.getConnection(System.getenv("DP_DB_HOSTNAME"),
                    System.getenv("DP_DB_USER"), System.getenv("DP_DB_PW"));
        } catch (SQLException e) {
            // Print the sql error.
            System.out.println(e.getMessage());
        }
    }

    /**
     * Returns the connection object.
     * @return  The connection abject.*/
    public Connection getConn() {
        return conn;
    }
}
