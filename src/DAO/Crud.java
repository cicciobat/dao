package DAO;

import java.io.IOException;
import java.util.List;

/**
 * {@code Interface Crud} allows to do simple CRUD function.
 *
 * @author Francesco Battaglia.
 * @param <T>   The type of object in this interface.
 * */
public interface Crud<T> {
    /**
     * Add method signature.
     * @return A boolean value.
     * */
    public boolean add(T object);
    /**
     * Find all method signature.
     * @return A list of T type.
     * */
    public List<T> findAll();
    /**
     * Update method signature.
     * @return A boolean value.
     * */
    public boolean update(T object);
    /**
     * Remove method signature.
     * @return A boolean value.
     * */
    public boolean remove(T object);
}
