package model;

import java.sql.Date;

/**
 * {@code Class Person} provides person objects.
 * @author Francesco Battaglia.
 * */
public class Person {
    private String name;
    private String surname;
    private Date dateOfBirth;
    private int age;
    private String address;

    public Person(String name, String surname, String dateOfBirth, String address) {
        this.name = name;
        this.surname = surname;
        this.address = address;
        try {
            // Assign a date value base on string value, uses a regex to standardize the date format.
            this.dateOfBirth = Date.valueOf(dateOfBirth.replaceAll("[.\\-/]", "-"));
        } catch (IllegalArgumentException e) {
            // Prints the correct date format to use.
            System.out.println("Date should in YYYY-MM-DD format.");
        }
    }

    public Person(String name, String surname, String dateOfBirth, int age, String address) {
        this.name = name;
        this.surname = surname;
        try {
            // Assign a date value base on string value, uses a regex to standardize the date format.
            this.dateOfBirth = Date.valueOf(dateOfBirth.replaceAll("[.\\-/]", "-"));
        } catch (IllegalArgumentException e) {
            // Prints the correct date format to use.
            System.out.println("Date should in YYYY-MM-DD format.");
        }
        this.age = age;
        this.address = address;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public Date getDateOfBirth() {
        return dateOfBirth;
    }

    public void setDateOfBirth(Date dateOfBirth) {
        this.dateOfBirth = dateOfBirth;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    @Override
    public String toString() {
        return "Person{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", dateOfBirth=" + dateOfBirth +
                ", age=" + age +
                ", address='" + address + '\'' +
                '}';
    }
}
