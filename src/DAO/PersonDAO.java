package DAO;

import controller.DatabaseConnection;
import model.Person;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;


/**
 * {@code Class PersonDao} implements {@code Crud<Person>} that allows to do simple CRUD function.
 * @author Francesco Battaglia.
 * @see DAO.Crud
 * */
public class PersonDAO implements Crud<Person> {

    /**
     * Adds a {@code Person} object to a database using SQL Query.
     * @param object    The person that will be added.
     * @return  True if new person was added, otherwise return false.
     * @exception SQLException  If something goes wrong during the database connection.
     * */
    @Override
    public boolean add(Person object) {
        // Create new database connection.
        DatabaseConnection conn = new DatabaseConnection();
        try {
            // The sql query.
            String insert = "INSERT INTO DESIGN_PATTERN.UTENTI(name, surname, " +
                    "date_of_birth, address) VALUES (?,?,?,?); ";
            // Prepare a sql statement.
            PreparedStatement stmt = conn.getConn().prepareStatement(insert);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getSurname());
            stmt.setDate(3, object.getDateOfBirth());
            stmt.setString(4, object.getAddress());
            // Performs the query.
            stmt.executeUpdate();
            // Closes the statement.
            stmt.close();
            // Closes the connection.
            conn.getConn().close();
            return true;
        } catch (SQLException e) {
            // Prints the sql error.
            System.out.println(e.getMessage());
            return false;
        }
    }

    /**
     * Finds all {@code Person} object in a database using SQL Query.
     * @return  The list of Person found, otherwise return an empty list..
     * @exception SQLException  If something goes wrong during the database connection.
     * */
    @Override
    public List<Person> findAll() {
        // Create new database connection.
        DatabaseConnection conn = new DatabaseConnection();
        // Create a new list where persone found in database will be added.
        List<Person> list = new ArrayList<>();
        try {
            // The sql query.
            String find = "SELECT * FROM DESIGN_PATTERN.UTENTI; ";
            // Prepare a sql statement.
            PreparedStatement stmt = conn.getConn().prepareStatement(find);
            // Result of statement query.
            ResultSet rs = stmt.executeQuery();
            // For each result of query adds a new Person to list.
            while (rs.next()) {
                list.add(new Person(rs.getString("name"), rs.getString("surname"),
                        rs.getDate("date_of_birth").toString(), rs.getInt("age"),
                        rs.getString("address")));
            }
            // Closes the statement.
            stmt.close();
            // Closes the connection.
            conn.getConn().close();
            return list;
        } catch (SQLException e) {
            // Prints the sql error.
            System.out.println(e.getMessage());
            return list;
        }
    }

    /**
     * Updates a {@code Person} object in a database using SQL Query. Use the person name.
     * @param object    The person that will be updated.
     * @return  True if new person was updated, otherwise return false.
     * @exception SQLException  If something goes wrong during the database connection.
     * */
    @Override
    public boolean update(Person object) {
        // Create new database connection.
        DatabaseConnection conn = new DatabaseConnection();
        try {
            // The sql query.
            String update = "UPDATE DESIGN_PATTERN.UTENTI SET name = ?, surname = ?, " +
                    "date_of_birth = ?, address = ? WHERE name = ?;";
            // Prepare a sql statement.
            PreparedStatement stmt = conn.getConn().prepareStatement(update);
            stmt.setString(1, object.getName());
            stmt.setString(2, object.getSurname());
            stmt.setDate(3, object.getDateOfBirth());
            stmt.setString(4, object.getAddress());
            stmt.setString(5, object.getName());
            // Performs the database update.
            stmt.executeUpdate();
            // Closes the statement.
            stmt.close();
            // Closes the connection.
            conn.getConn().close();
            return true;
        } catch (SQLException e) {
            // Prints the sql error.
            System.out.println(e.getMessage());
            return false;
        }
    }

    /**
     * Removes a {@code Person} object to a database using SQL Query. Use the person name.
     * @param object    The person that will be removed.
     * @return  True if new person was removed, otherwise return false.
     * @exception SQLException  If something goes wrong during the database connection.
     * */
    @Override
    public boolean remove(Person object) {
        // Create new database connection.
        DatabaseConnection conn = new DatabaseConnection();
        try {
            // The sql query.
            String delete = "DELETE FROM DESIGN_PATTERN.UTENTI WHERE name = ?; ";
            // Prepare a sql statement.
            PreparedStatement stmt = conn.getConn().prepareStatement(delete);
            stmt.setString(1, object.getName());
            // Performs the deletion from database.
            stmt.executeUpdate();
            // Closes the statement.
            stmt.close();
            // Closes the connection.
            conn.getConn().close();
            return true;
        } catch (SQLException e) {
            // Prints the sql error.
            System.out.println(e.getMessage());
            return false;
        }
    }
}
